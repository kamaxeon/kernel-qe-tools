# CentOS Stream Kernel QE Tools

Repository to group all command line tools for Kernel QE.

## Installation

Just use `pip`

```shell
python3 -m pip install git+https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools
```

## Test

To run tests, just type `tox` locally, you can also tests in a podman container via

```shell
podman run --pull always --rm -it --volume .:/code:Z --workdir /code quay.io/cki/cki-tools:production:latest tox
```

## Detailed documentation of Python tools

| Tool                                            | Detailed documentation                                                          |
|-------------------------------------------------|---------------------------------------------------------------------------------|
| `bkr2kcidb`       | [documentation](docs/README.bkr2kcidb.md)       |
| `find_compose_pkg`       | [documentation](docs/README.find_compose_pkg.md)       |
| `get_repository_report`       | [documentation](docs/README.get_repository_report.md)       |
