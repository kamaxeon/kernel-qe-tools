"""Beaker parser to kcidb."""

import argparse
from datetime import datetime
from email.utils import parseaddr
import re

from cki_lib.kcidb.validate import sanitize_kcidb_status
from cki_lib.misc import utc_now_iso

DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
NVR_RE = r"(kernel(?:-rt|-automotive|-64k)?)(?:-debug)?-(\d.*)"
EMAIL_RE = r"([A-Za-z0-9]+[.\-_]?)*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+"


class ActionNotFound(Exception):
    """Raised when an action is not found."""


def clean_dict(data):
    """Remove keys with empty value from a dict."""
    return {
        key: value
        for key, value in data.items()
        if isinstance(value, bool) or value
    }


def sanitize_name(name):
    """Sanitize name."""
    characters_to_be_sanitized = ['/', ' ']

    if name.startswith(tuple(characters_to_be_sanitized)):
        name = name[1:]

    for character in characters_to_be_sanitized:
        name = name.replace(character, '_')

    return name.replace('__', '_')


def get_seconds(time_str):
    """Get seconds from time."""
    hours, minutes, seconds = time_str.split(':')
    return int(hours) * 3600 + int(minutes) * 60 + int(seconds)


def get_utc_datetime(datetime_str):
    """Get datetime in UTC format."""
    if not datetime_str:
        return utc_now_iso()
    return f'{datetime.strptime(datetime_str, DATE_FORMAT).isoformat()}+00:00'


def get_int(text):
    """Cast to int when it's possible."""
    try:
        return int(text)
    except (ValueError, TypeError):
        return None


def get_nvr_info(nvr):
    """Get a tuple with package_name and kernel_version, or return Nones if not found."""
    if result := re.search(NVR_RE, nvr):
        return result.group(1), result.group(2)
    return None, None


def get_console_log_url(recipe_id):
    """Get console log."""
    return f'https://beaker.engineering.redhat.com/recipes/{recipe_id}/logs/console.log'


def get_recipe_url(recipe_id):
    """Get recipe url."""
    return f'https://beaker.engineering.redhat.com/recipes/{recipe_id}'


def get_provenance_info(function, url, service_name):
    """Get provenance info."""
    if url:
        return clean_dict({
            'function': function,
            'url': url,
            'service_name': service_name
        })
    return {}


def get_brew_url(brew_id):
    """Get brew url."""
    return f'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID={brew_id}'


def get_output_console_log(recipe_id):
    """Get console log in a list."""
    return [{'name': 'console.log', 'url': get_console_log_url(recipe_id)}]


def email_type(value):
    """Check an email address."""
    _, address = parseaddr(value)
    if not re.fullmatch(EMAIL_RE, address):
        raise argparse.ArgumentTypeError(f"'{value}' is not a valid email address.")
    return value


def sanitize_test_status(task):
    # pylint: disable=line-too-long
    """
    Get test status from a Beaker Task.

    TODO: Wait until https://gitlab.com/cki-project/datawarehouse/-/issues/306 is ready
    (Add support to Datawarehouse).

    At cki-lib, we have a helper function to get the status
    giving the task status, but in bkr2kcidb we need to manage
    the MISS status.

    When Beaker did not run some tests, we have to assign the MISS status,
    otherwise we'll use the cki_lib helper function.

    A MISS test is a Beaker task without *start_time*, the result is "Warn" and status is "Aborted",
    and it only has a result.

    Below we can see an example

    <task name="task_1" role="None" version="1-33" id="10" result="Warn" status="Aborted" avg_time="7200" start_time="2023-08-28 21:50:26" finish_time="2023-08-29 00:20:55" duration="2:30:29">
      <rpm name="package_name_1" path="/mnt/tests/kernel/test1"/>
        <roles>
          <role value="None">
            <system value="server-a.redhat.com"/>
          </role>
        </roles>
        <logs>
          <log href="https://beaker.server/recipes/1/tasks/10/logs/harness.log" name="harness.log"/>
          <log href="https://beaker.server/recipes/1/tasks/10/logs/taskout.log" name="taskout.log"/>
        </logs>
        <results>
          <result path="path1" start_time="2023-08-28 21:54:03" score="None" result="Pass" id="1">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/1/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/1/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="path_2" start_time="2023-08-28 21:54:13" score="None" result="Pass" id="2">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/2/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/2/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="path_3" start_time="2023-08-28 21:54:23" score="None" result="Pass" id="3">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/3/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/3/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="path_4" start_time="2023-08-28 21:54:34" score="None" result="Pass" id="4">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/4/logs/dmesg.log" name="dmesg.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/4/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/4/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="/" start_time="2023-08-29 00:20:55" score="0" result="Warn" id="5">External Watchdog Expired</result>
        </results>
      </task>
      <task name="git://git_server#general/include" role="None" id="11" result="Warn" status="Aborted">
        <fetch url="git://git_server#general/include"/>
        <roles>
          <role value="None">
            <system value="server-b.redhat.com"/>
          </role>
        </roles>
        <params>
          <param name="CMD" value="script.sh"/>
          <param name="INSTALL" value="something"/>
        </params>
        <logs/>
        <results>
          <result path="/" start_time="2023-08-29 00:20:55" score="0" result="Warn" id="6">External Watchdog Expired</result>
        </results>
      </task>

    The task number 10, did not finish correctly due to the External Watchdog and the number 11
    did not start, we don't have any *start_time* and only the External Watchdog result.

    Based on this XML code, we should mark the task number 11 as MISS.

    Here we can see other example:

      <task name="UPT smoke tests aborting the recipe" role="STANDALONE" id="12" result="Fail" status="Aborted" start_time="2023-09-18 13:01:23" finish_time="2023-09-18 13:01:35" duration="0:00:12">
        <fetch url="https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/archive/main/kernel-tests-main.zip#upt_smoke_tests/abort_full_recipe"/>
        <roles>
          <role value="STANDALONE">
            <system value="sut.redhat.com"/>
          </role>
        </roles>
        <logs>
          <log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/logs/taskout.log" name="taskout.log"/>
          <log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/logs/harness.log" name="harness.log"/>
        </logs>
        <results>
          <result path="Setup" start_time="2023-09-18 13:01:28" score="0" result="Pass" id="19">None<logs><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/19/logs/avc.log" name="avc.log"/><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/19/logs/resultoutputfile.log" name="resultoutputfile.log"/></logs></result>
          <result path="First-test" start_time="2023-09-18 13:01:30" score="0" result="Pass" id="777099241">None<logs><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099241/logs/avc.log" name="avc.log"/><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099241/logs/resultoutputfile.log" name="resultoutputfile.log"/></logs></result>
          <result path="UPT smoke tests aborting the recipe" start_time="2023-09-18 13:01:32" score="1" result="Fail" id="777099247">None<logs><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099247/logs/avc.log" name="avc.log"/><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099247/logs/resultoutputfile.log" name="resultoutputfile.log"/></logs></result>
          <result path="/" start_time="2023-09-18 13:01:35" score="0" result="Warn" id="20">None</result>
        </results>
      </task>
      <task name="Sleep 60 seconds" role="STANDALONE" id="13" result="Warn" status="Aborted">
        <fetch url="https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/archive/main/kernel-tests-main.zip#distribution/command"/>
        <roles>
          <role value="STANDALONE">
            <system value="sut.redhat.com"/>
          </role>
        </roles>
        <params>
          <param name="TESTARGS" value="sleep 60"/>
        </params>
        <logs>
          <log href="https://beaker.engineering.redhat.com/recipes/10/tasks/13/logs/harness.log" name="harness.log"/>
        </logs>
        <results>
          <result path="/" start_time="2023-09-18 13:01:36" score="0" result="Warn" id="21">None</result>
        </results>
      </task>

    In this recipe has been aborted by the task 12, and the task number 13 never has been executed.
    The task 13, does not have *start_time* and the status and result are the expected ones with one
    result.
    """  # noqa
    # Check for miss status
    results = task.findall('.//result')
    if not task.get('start_time') and \
            task.get('result') == 'Warn' and \
            task.get('status') == 'Aborted' and \
            len(results) == 1:
        return 'MISS'

    # No MISS status, use the cki_lib helper
    return sanitize_kcidb_status(task.get('result'))
