"""Dataclasses."""
from dataclasses import dataclass
import typing


@dataclass
class ExternalOutputFile:
    """External OutputFile."""

    name: str
    url: str


@dataclass
class ParserArguments:
    """Arguments to pass to a bkr2kcidb parser.

    All external arguments should be here
    """

    # pylint: disable=too-many-instance-attributes
    brew_task_id: str
    checkout: str
    debug: bool
    extra_output_files: typing.Optional[typing.List[ExternalOutputFile]]
    kernel_version: str
    origin: str
    package_name: str
    test_plan: bool
    tests_provisioner_url: str
    contacts: typing.List[str]
