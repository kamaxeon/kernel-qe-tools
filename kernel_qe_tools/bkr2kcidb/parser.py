"""Beaker parser to kcidb."""

from dataclasses import asdict
from email.utils import parseaddr
import re
import xml.etree.ElementTree as ET

from cki_lib.kcidb import KCIDBFile
from cki_lib.kcidb import sanitize_kcidb_status
from cki_lib.logger import get_logger

from . import utils

LOGGER = get_logger(__name__)


class Bkr2KCIDBParser:
    """Parser."""

    def __init__(self, bkr_content, args):
        """Initialize object with the given data."""
        self.root = ET.fromstring(bkr_content)
        self.args = args
        self.report = None
        self._clean_report()

    def exist_build_by_arch(self, arch):
        """Check if the build by architecture has been previously stored."""
        for build in self.report['builds']:
            if build.get('id') == self._get_build_id(arch):
                return True
        return False

    @property
    def checkout_id(self):
        """Get checkout id."""
        return utils.sanitize_name(f'{self.args.origin}:{self.args.checkout}')

    def _get_build_id(self, arch):
        """Generate the build id given an architecture."""
        build_id = utils.sanitize_name(f'{self.checkout_id}_{arch}_{self.args.package_name}')
        if self.args.debug:
            build_id += '_debug'
        return build_id

    def _get_test_id(self, arch, task_id):
        """Generate test id given architecture and test name."""
        return utils.sanitize_name(f'{self._get_build_id(arch)}_bkr2kcidb_{task_id}')

    def _clean_report(self):
        """Clean report."""
        self.report = {
            'checkout': None,
            'builds': [],
            'tests': []
        }

    def add_checkout(self):
        """Add checkout."""
        self.report['checkout'] = utils.clean_dict({
            'id': self.checkout_id,
            'origin': self.args.origin,
            'tree_name': self.get_tree_name(),
            'valid': True,
            'misc': self.add_checkout_misc_info()
        })

    def add_build(self, arch):
        """Add build if it is not present."""
        if not self.exist_build_by_arch(arch):
            self.report['builds'].append({
                'id': self._get_build_id(arch),
                'origin': self.args.origin,
                'checkout_id': self.checkout_id,
                'architecture': arch,
                'valid': True,
                'misc': self.add_build_misc_info()
            })

    def get_extra_output_files(self):
        """Append output passed by cli."""
        return [asdict(output_file) for output_file in self.args.extra_output_files]

    @staticmethod
    def get_output_files(logs):
        """Return output_files from a log section."""
        output_files = []
        if logs:
            for log in logs.iter('log'):
                output_files.append({
                    'name': log.get('name'),
                    'url': log.get('href')
                })
        return output_files

    def get_results(self, results, test_name, test_status):
        """
        Generate results for a test.

        We can process directly all results except results added by Beaker on
        top the Restraint results.

        When a test is Aborted, Beaker always add a result following this criteria:
        * The path field is equal to '/'
        * The result field is equal to 'Warn'
        * The result contains a text with the Beaker Information

        If a status of the test is 'MISS', all subtest will use 'MISS' as well.
        """
        all_results = []
        for number, result in enumerate(results.iter('result'), 1):
            path = result.get('path')
            _result = result.get('result')

            if path == '/' and _result in ['Warn', 'Panic']:
                name = result.text
            else:
                name = path

            if test_status == 'MISS':
                subtest_status = test_status
                LOGGER.info('Forcing subtest(%s).status=MISS because test.status=MISS',
                            f'{test_name}.{number}')
            else:
                subtest_status = sanitize_kcidb_status(_result)

            all_results.append(utils.clean_dict({
                'id': f'{test_name}.{number}',
                'name': name or 'UNDEFINED',
                'status': subtest_status,
                'output_files': self.get_output_files(result.find('logs')),
            }))

        return all_results

    def add_checkout_misc_info(self):
        """Return misc for a checkout."""
        misc = {}

        misc['kernel_version'] = self.args.kernel_version
        misc['public'] = False
        if self.args.brew_task_id:
            brew_task_url = utils.get_brew_url(self.args.brew_task_id)
            misc['provenance'] = [utils.get_provenance_info('executor',
                                                            brew_task_url,
                                                            'buildsystem')]

        return utils.clean_dict(misc)

    def add_build_misc_info(self):
        """Return misc for a build."""
        misc = {}

        misc['package_name'] = self.args.package_name
        if self.args.brew_task_id:
            brew_task_url = utils.get_brew_url(self.args.brew_task_id)
            misc['provenance'] = [utils.get_provenance_info('executor',
                                                            brew_task_url,
                                                            'buildsystem')]

        return utils.clean_dict(misc)

    def add_test_misc_info(self, recipe_id, test_id, task, test_status):
        """Return misc for a test."""
        misc = {}

        results = task.find('results')
        if results:
            misc['results'] = self.get_results(results, test_id, test_status)

        beaker_url = utils.get_recipe_url(recipe_id)
        misc['provenance'] = [utils.get_provenance_info('executor', beaker_url, 'beaker')]
        if self.args.tests_provisioner_url:
            # Assuming all test provisioners come from jenkins
            misc['provenance'].append(utils.get_provenance_info('provisioner',
                                                                self.args.tests_provisioner_url,
                                                                'jenkins'))
        maintainers = []
        for contact in self.args.contacts:
            name, email = parseaddr(contact)
            maintainers.append({
                'name': name or email,
                'email': email

            })

        misc['maintainers'] = maintainers

        return utils.clean_dict(misc)

    def add_test(self, arch, recipe_id, recipe_system, task):
        """Add test."""
        test = {
            'id': self._get_test_id(arch, task.get("id")),
            'origin': self.args.origin,
            'build_id': self._get_build_id(arch),
            'comment': task.get('name'),
            'path': utils.sanitize_name(task.get('name')),
        }
        if not self.args.test_plan:
            test['start_time'] = utils.get_utc_datetime(task.get('start_time'))
            test['duration'] = utils.get_seconds(task.get('duration', '00:00:00'))
            test['output_files'] = self.get_output_files(task.find('logs')) + \
                self.get_extra_output_files() + \
                utils.get_output_console_log(recipe_id)
            test['status'] = utils.sanitize_test_status(task)
            test['misc'] = self.add_test_misc_info(recipe_id, test['id'], task, test['status'])

        test['environment'] = utils.clean_dict({'comment': recipe_system})

        self.report['tests'].append(utils.clean_dict(test))

    def get_tree_name(self):
        """Get tree_name from the first distro field of any recipe."""
        regxep = re.compile(r'(rhel-\d+\.\d+)')
        for recipe in self.root.iter('recipe'):
            if 'distro' in recipe.attrib:
                tree_name = recipe.get('distro').lower()
                re_match = regxep.match(tree_name)
                if re_match:
                    return re_match.group(1)
        return None

    def process(self):
        """Iterate all recipe and generate the report."""
        self._clean_report()
        self.add_checkout()
        for recipe in self.root.iter('recipe'):
            arch = recipe.get('arch')
            recipe_id = recipe.get('id')
            recipe_system = recipe.get('system')
            self.add_build(arch)
            for task in recipe.iter('task'):
                self.add_test(arch, recipe_id, recipe_system, task)

    def write(self, path):
        """Write info."""
        kcidb_file = KCIDBFile(path)

        # Checkout
        kcidb_file.set_checkout(self.report['checkout']['id'], self.report['checkout'])

        # Builds
        for build in self.report['builds']:
            kcidb_file.set_build(build['id'], build)

        # Tests
        for test in self.report['tests']:
            kcidb_file.set_test(test['id'], test)

        kcidb_file.save()
