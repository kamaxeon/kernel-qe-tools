# Get repository report

## Getting started

This utility generates a json files to be consumed by other CI jobs.
The report can use all files or only changes between two commits.

Currently, it's creating those main categories:

* `git`
* `markdown_files`
* `shell_files`
* `shellspec_directories`
* `tmt_directories`
* `yamllint_files`
* `python_files`

## Usage

```bash
usage: get_repository_report [-h] [--first-commit FIRST_COMMIT] [--last-commit LAST_COMMIT] [--output-file OUTPUT_FILE] [--working-dir WORKING_DIR]

Generate a report to be used in other CI jobs.

optional arguments:
  -h, --help            show this help message and exit
  --first-commit FIRST_COMMIT
                        SHA of the last commit, ommit it to use all files.
  --last-commit LAST_COMMIT
                        SHA of the last commit, ommit it to use all files.
  --output-file OUTPUT_FILE
                        Output file (By default ci_files.json).
  --working-dir WORKING_DIR
                        Git working directory (By default the current directory).
```
