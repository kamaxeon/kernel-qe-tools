"""Test Clean Dict."""
import os
import pathlib
import shutil
import tempfile
import unittest

from kernel_qe_tools.ci_tools import get_repository_report

CHANGES = [
    ['M', 'dir1/dir2/README.md'],
    ['D', 'dir1/dir2/OLD_README.md'],
    ['M', 'dir3/config.yml'],
    ['A', 'dir4/spec/test_spec.sh'],
    ['A', 'dir4/.shellspec'],
    ['M', 'dir6/package/file_2.py']
]

FILES = [
    '.fmf',
    'dir1/dir2/file.fmf',
    'dir1/dir2/script.sh',
    'dir1/dir2/README.md',
    'dir1/dir2/conf/test_config.yml',
    'dir3/spec/some_file.sh',
    'dir3/README.md',
    'dir3/config.yml',
    'dir4/spec/test_spec.sh',
    'dir4/.shellspec',
    'dir4/run_test.sh',
    'dir5/test.fmf',
    'dir5/run_test.sh',
    'dir5/spec/test_spech.sh',
    'dir5/.shellspec',
    'dir6/file_1.py',
    'dir6/package/file_2.py'
]

DIRECTORIES = ['dir1/dir2/conf', 'dir3/spec', 'dir4/spec', 'dir5/spec', 'dir6/package']


class TestGetRepositoryReport(unittest.TestCase):
    """Test Get Repository Report."""

    @classmethod
    def setUpClass(cls):
        """Prepare directories and files."""
        cls.old_path = os.getcwd()
        cls.temp_path = tempfile.mkdtemp()

        os.chdir(cls.temp_path)
        # Create directories
        for directory in DIRECTORIES:
            pathlib.Path(directory).mkdir(parents=True)

        # Create files
        for file in FILES:
            pathlib.Path(file).touch()

    @classmethod
    def tearDownClass(cls):
        """Cleaning up directories."""
        os.chdir(cls.old_path)
        shutil.rmtree(cls.temp_path)

    def test_get_files_by_extensions(self):
        """Test get files."""
        cases = (
            ('Getting only mr changes', ('.md', '.MD'), CHANGES, ['dir1/dir2/README.md']),
            ('Getting all files', ('.md', '.MD'), None, ['dir1/dir2/README.md', 'dir3/README.md'])

        )
        for (description, extensions, changes, expected) in cases:
            with self.subTest(description):
                self.assertCountEqual(expected,
                                      get_repository_report.get_files_by_extensions(
                                          FILES, changes, extensions
                                      ))

    def test_get_tmt_directories(self):
        """Test tmt directories."""
        cases = (
            ('Getting only mr changes', CHANGES, ['dir1/dir2']),
            ('Getting all files', None, ['dir1/dir2', 'dir5'])
        )

        for (description, changes, expected) in cases:
            with self.subTest(description):
                self.assertCountEqual(expected,
                                      get_repository_report.get_tmt_directories(FILES, changes))

    def test_get_shellspec_directories(self):
        """Test shellspec directories."""
        cases = (
            ('Getting only mr changes', CHANGES, ['dir4']),
            ('Getting all files', None, ['dir4', 'dir5'])
        )

        for (description, changes, expected) in cases:
            with self.subTest(description):
                self.assertCountEqual(expected,
                                      get_repository_report.get_shellspec_directories(
                                          FILES, changes
                                      ))

    def test_get_git_changes(self):
        """Test git changes."""
        expected_with_changes = {
            'modified': ['dir1/dir2/README.md', 'dir3/config.yml', 'dir6/package/file_2.py'],
            'copy-edit': [],
            'rename-edit': [],
            'added': ['dir4/spec/test_spec.sh', 'dir4/.shellspec'],
            'deleted': ['dir1/dir2/OLD_README.md'],
            'unmerged': []
        }
        expected_all_files = {
            'modified': [],
            'copy-edit': [],
            'rename-edit': [],
            'added': [],
            'deleted': [],
            'unmerged': []
        }

        cases = (
            ('Getting only mr changes', CHANGES, expected_with_changes),
            ('Getting all files', None, expected_all_files),
        )
        for (description, changes, expected) in cases:
            with self.subTest(description):
                self.assertDictEqual(expected,
                                     get_repository_report.get_git_changes(changes))

    def test_get_report_with_changes(self):
        """Test git report with changes."""
        report = get_repository_report.get_report(FILES, CHANGES)

        self.assertEqual('only_mr_changes', report['data']['type'])
        self.assertEqual(3, len(report['data']['git']['modified']))
        self.assertEqual(0, len(report['data']['git']['copy-edit']))
        self.assertEqual(0, len(report['data']['git']['rename-edit']))
        self.assertEqual(2, len(report['data']['git']['added']))
        self.assertEqual(1, len(report['data']['git']['deleted']))
        self.assertEqual(1, len(report['data']['shell_files']))
        self.assertEqual(1, len(report['data']['tmt_directories']))
        self.assertEqual(1, len(report['data']['markdown_files']))
        self.assertEqual(1, len(report['data']['yaml_files']))
        self.assertEqual(1, len(report['data']['shellspec_directories']))
        self.assertEqual(1, len(report['data']['python_files']))

    def test_get_report_with_all_files(self):
        """Test git report with all files."""
        report = get_repository_report.get_report(FILES, None)

        self.assertEqual('all_files', report['data']['type'])
        self.assertEqual(0, len(report['data']['git']['modified']))
        self.assertEqual(0, len(report['data']['git']['copy-edit']))
        self.assertEqual(0, len(report['data']['git']['rename-edit']))
        self.assertEqual(0, len(report['data']['git']['added']))
        self.assertEqual(0, len(report['data']['git']['deleted']))
        self.assertEqual(6, len(report['data']['shell_files']))
        self.assertEqual(2, len(report['data']['tmt_directories']))
        self.assertEqual(2, len(report['data']['markdown_files']))
        self.assertEqual(2, len(report['data']['yaml_files']))
        self.assertEqual(2, len(report['data']['shellspec_directories']))
        self.assertEqual(2, len(report['data']['python_files']))
