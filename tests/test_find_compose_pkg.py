"""Test Find Compose Pkg"""

import io
import unittest
from unittest import mock

from kernel_qe_tools.ci_tools import find_compose_pkg


class TestFindComposePkg(unittest.TestCase):
    """Test Find Compose Pkg."""

    # pylint: disable=too-many-arguments
    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def assert_stdout(self, compose, pkg, expected_output, verbose, mock_stdout):
        find_compose_pkg.rhel_package(compose, pkg, verbose)
        self.assertEqual(mock_stdout.getvalue().rstrip(), expected_output)

    def test_get_kernel_from_compose(self):
        # pylint: disable=no-value-for-parameter
        self.assert_stdout('RHEL-9.0.0-20220420.0', None, 'kernel-5.14.0-70.13.1.el9_0', False)

    def test_get_gcc_from_compose_id(self):
        # pylint: disable=no-value-for-parameter
        self.assert_stdout('RHEL-8.6.0', ['gcc'], 'gcc-8.5.0-10.el8', False)

    def test_verbose_output_pkg_arch(self):
        # pylint: disable=no-value-for-parameter
        self.assert_stdout('RHEL-8.8.0-20230411.3', ['rt-tests'],
                           "AppStream: rt-tests-2.5-1.el8 ['x86_64']", True)

    def test_verbose_output_no_pkg_arch(self):
        # pylint: disable=no-value-for-parameter
        self.assert_stdout('RHEL-8.8.0-20230411.3', ['rtla'],
                           "AppStream: rtla-5.14.0-4.el8", True)

    def test_package_not_found(self):
        result = find_compose_pkg.rhel_package('RHEL-8.8.0', ['foobar'], True)
        self.assertNotEqual(result, 0)

    def test_compose_not_found(self):
        result = find_compose_pkg.rhel_package('RHEL-8.8.0-12345678', ['kernel'], True)
        self.assertNotEqual(result, 0)

    def test_bad_compose_id_short(self):
        result = find_compose_pkg.rhel_package('RHEL-1.2', ['kernel'], True)
        self.assertNotEqual(result, 0)

    def test_bad_compose_id_long(self):
        result = find_compose_pkg.rhel_package('RHEL-7.9-99999999.0', ['kernel'], True)
        self.assertNotEqual(result, 0)

    def test_find_compose_pkg_main_valid(self):
        result = find_compose_pkg.main(['-c', 'RHEL-9.2.0', '-p', 'kernel-rt', '-v'])
        self.assertEqual(result, 0)

    def test_find_compose_pkg_main_no_compose(self):
        result = find_compose_pkg.main(['-v'])
        self.assertEqual(result, 4)
